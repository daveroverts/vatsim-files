<?php
header( "refresh:125;url=route.php" );

	$servers = [
    1 => "http://info.vroute.net/vatsim-data.txt",
    2 => "http://vatsim.aircharts.org/vatsim-data.txt",
    3 => "http://vatsim-data.hardern.net/vatsim-data.txt",
    4 => "http://wazzup.openaviationdata.com/vatsim/vatsim-data.txt"
    ]; //Creates an array that has all the vatsim-data.txt files

    $my_var = file_get_contents($servers[random_int(1,4)]); //using a random integer, one of the servers will be used for data extraction
	$pieces = explode("\n", $my_var);
	$findme = ($_GET["id"]); //VATSIM ID
	$output = "";

    if (!isset($findme)) {
        echo "No VATSIM ID has been entered, please try again!";
        exit;
    }

	foreach ($pieces as $s) {
     	$pos = strpos($s, $findme);

     	if ($pos == true)
     	{
     		$record = explode(":", $s);
			$output = $output . "DEP: " . $record[getPos("planned_depairport")] . " | ";
			$output = $output . "ARR: " . $record[getPos("planned_destairport")] . " | ";
			$output = $output . "ROUTE: " . $record[getPos("planned_route")] . " | ";
     	}
 	}

 	if ($output == "") {
 		echo "Aircraft could not be found. If you just logged on, please wait up 1-2 minute(s).";
 	}

	echo $output;

	function getPos($field)
	{
		$fields = explode(":", "callsign:cid:realname:clienttype:frequency:latitude:longitude:altitude:groundspeed:planned_aircraft:planned_tascruise:planned_depairport:planned_altitude:planned_destairport:server:protrevision:rating:transponder:facilitytype:visualrange:planned_revision:planned_flighttype:planned_deptime:planned_actdeptime:planned_hrsenroute:planned_minenroute:planned_hrsfuel:planned_minfuel:planned_altairport:planned_remarks:planned_route:planned_depairport_lat:planned_depairport_lon:planned_destairport_lat:planned_destairport_lon:atis_message:time_last_atis_received:time_logon:heading:QNH_iHg:QNH_Mb:");
		$max = sizeof($fields);
		$ret = 0;

		for ($i = 0; $i<$max; $i++)
		{
			if ($fields[$i] == $field)
			{
				$ret = $i;
			}
		}
		return $ret;
	}

?>