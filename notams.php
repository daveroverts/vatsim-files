<?php

$notams = [
    1 => "Flying from EHAM? Make sure that you use one if the following exit points as first waypoints: ANDIK, ARNEM, BERGI, EDUPO, GORLO, LEKKO, LOPIK",
    2 => "Remember, EHAM has 2 main taxiways, Alpha, and Bravo. Alpha is Clockwise, while Bravo is Counter-Clockwise!",
    3 => "Remember, there is a Auto-Handoff for all departures, and arrivals at EHAM (unless told, or noted otherwise)! All departures are to contact EHAM_W_APP 121.200. Arrivals are to contact EHAM_N_GND 121.800.",
    4 => "Remember, FL250 is NOT available within the EHAA FIR/Netherlands, same applies for Germany, and Belgium."
];

$random_getal = random_int(1,4);

echo "Notam " . $random_getal . ": " . $notams[$random_getal];
?>